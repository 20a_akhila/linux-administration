# Linux Administration

## Contents of Course

### Day 1

- What is Linux
- Philosophy of Linux
- Linux Terminologies 
- Linux Distributions
- Bootup Process and exlpaining about each step in that
    - BIOS
    - RAM Disk
    - Boot Loader
    - Linux Kernel etc.,
        - Distribution Roles
- Systemd
- Linux File System Basics
    - Partitions and Filesystems
    - Filesystem Hierarchy Standards
    - 
- Linux Distribution Installation
    - Questions to ask while choosing a Distribution

### Day 2

- Basic Linux Commands
- What is Shell?
- What is Shell Scripting?
- Importance of Shell Scripting Today! (Why to learn)
- Types of Shells
- Scripting Standards
    - Naming Convention
    - Script File Permissions
    - Shell Script Format
- Hello World program
- Trying to execue basic linux commands using script (Basic Linux Commands)
- Defining Variables
- Conditional Statements
- Loops
- Activity
    - Accessing data from a File
    - Check remote servers connectivity
    - Take user input from arguments and print the variables
    - Compare two number using conditional statements
    - Script Scheduling and Notification
    - Script to delete old files
    - Backup Filesystem
    - Copy files to remote System
    - User Directory Assignment
    - List of Users Logged in by date
    - User account management
    - Check Process Status and killing it
    - Create System Inventory
    - 
